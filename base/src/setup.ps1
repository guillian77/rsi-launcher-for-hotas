# - Hide the console -
$t = '[DllImport("user32.dll")] public static extern bool ShowWindow(int handle, int state);'
add-type -name win -member $t -namespace native
[native.win]::ShowWindow(([System.Diagnostics.Process]::GetCurrentProcess() | Get-Process).MainWindowHandle, 0)

[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
Add-Type -AssemblyName System.Windows.Forms

$dirScript = [Environment]::CurrentDirectory
$dirConfig = "$dirScript\config.ini"
$dirDesktop = [Environment]::GetFolderPath('Desktop')
$WScriptShell = New-Object -ComObject WScript.Shell

function Message
{
    Param ([string]$text, [string]$title)
    [System.Windows.Forms.MessageBox]::Show("$text", "$title")
}

function Debug
{
    Param ([String]$entry)

    Message -text $entry -title "Debug"

    exit
}

function SelectAFile
{
    Param ([string]$fileName, [string]$filters)

    if(!$filters)
    {
        $filters = "All files (*.*)|*.*"
    }

    $fileLocation = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
        InitialDirectory = $dirDesktop
        Title = "Select your $fileName"
        Filter = "$filters"
    }

    $dialogResult = $fileLocation.ShowDialog()
    $fileLocation = $fileLocation.FileName

    if($dialogResult -eq "Cancel")
    {
        Message -text "You should specify your $fileName location." -title "Error"
        exit
    }

    return $fileLocation
}

$SCLAUNCHER = SelectAFile -fileName "RSI Launcher.exe" -filters "RSI Launcher (*.exe)|*.exe"
$TARGET = SelectAFile -fileName "TARGETGUI.exe" -filters "TARGETGUI (*.exe)|*.exe"
$PROFILE = SelectAFile -fileName "T.A.R.G.E.T profile (fcf)"

$iniTARGET = "TARGET=$TARGET"
$iniPROFILE = "PROFILE=$PROFILE"
$iniSCLAUNCHER = "SCLAUNCHER=$SCLAUNCHER"

New-Item -path $dirScript -Name 'config.ini' -ItemType file -force
'# Configuration file' | Add-Content $dirConfig
$iniTARGET | Add-Content $dirConfig
$iniPROFILE | Add-Content $dirConfig
$iniSCLAUNCHER | Add-Content $dirConfig

$Shortcut = $WScriptShell.CreateShortcut("$dirDesktop\RSI Launcher for HOTAS.lnk")
$Shortcut.TargetPath = "$dirScript\launcher.exe"
$Shortcut.Save()

[System.Windows.Forms.MessageBox]::Show("A shortcut has been created on your Desktop.", "Success")
