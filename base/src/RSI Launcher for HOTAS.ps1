# ---------------------------------
# - STAR CITIZEN HOTAS HELP SCRIPT
# ---------------------------------
# - Editor: Aufrere Guillian
# - Last update: 2020/01/14

##############################
# BEFORE CODE
##############################
# - Hide the console -
$t = '[DllImport("user32.dll")] public static extern bool ShowWindow(int handle, int state);'
add-type -name win -member $t -namespace native
[native.win]::ShowWindow(([System.Diagnostics.Process]::GetCurrentProcess() | Get-Process).MainWindowHandle, 0)

##############################
# LOADS
##############################
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
Add-Type -AssemblyName System.Windows.Forms

##############################
# FUNCTIONS
##############################
function Message
{
    Param ([string]$text, [string]$title)
    [System.Windows.Forms.MessageBox]::Show("$text", "$title")
}

function Debug
{
    Param ([String]$entry)
    Message -text $entry -title "Debug"
    exit
}

function checkFilePresence($path, $message)
{
    if (!(Test-Path $path)) {
        Message -text "File $path not found." -title "ERROR: Not found"
        exit
    }
}

##############################
# VARIABLES
##############################
# Get the path from source executable
$FullPathToEXE = [System.Diagnostics.Process]::GetCurrentProcess().MainModule.FileName

# Get parent directory of source executable
$FullPathToDir = (get-item $FullPathToEXE).Directoryname

# Get the config.ini path
$dirConfig = "$FullPathToDir\config.ini"
checkFilePresence($dirConfig)

# Get config.ini content
$ConfigFileContent = Get-Content "$dirConfig"

# PATH: StarCitizen Launcher
$pathScLauncher = $ConfigFileContent | Where-Object { $_ -match 'SCLAUNCHER' }
$pathScLauncher = $pathScLauncher.Split('=')[1]

# PATH: T.A.R.G.E.T GUI
$pathTarget = $ConfigFileContent | Where-Object { $_ -match 'TARGET' }
$pathTarget = $pathTarget.Split('=')[1]

# PATH: T.A.R.G.E.T Profile
$pathTargetProfile = $ConfigFileContent | Where-Object { $_ -match 'PROFILE' }
$pathTargetProfile = $pathTargetProfile.Split('=')[1]

function verifyPresence()
{
    checkFilePresence($pathScLauncher, "RSI Launcher not found under " + $pathScLauncher)
    checkFilePresence($pathTarget, "T.A.R.G.E.T software not found under " + $pathTarget)
    checkFilePresence($pathTargetProfile, "T.A.R.G.E.T Profil not found under " + $pathTargetProfile)
}

##############################
# THE CODE LOGIC
##############################
# Define paths above.
verifyPresence

# Find running tasks.
$IsRunningSc = (tasklist | find /i "RSI Launcher.exe")
$IsRunningTarget = (tasklist | find /i "TARGETGUI.exe")

# Check T.A.R.G.E.T or StartCitizen Launcher
if ($IsRunningSc -or $IsRunningTarget)
{
    taskkill /im "RSI Launcher.exe" /F
    taskkill /im TARGETGUI.exe /F
}
else
{
    # Launch T.A.R.G.E.T profile
    & "$pathTarget" -r "$pathTargetProfile"

    # # Look for Thrustmaster devices are mounted
    $deviceStatus = Get-PnpDevice -Class Thrustmaster -Status OK -erroraction 'silentlycontinue'
    Do{
        Start-Sleep -Seconds 1.5
        $deviceStatus = Get-PnpDevice -Class Thrustmaster -Status OK -erroraction 'silentlycontinue'
    }Until($deviceStatus)

    Start-Process -FilePath "$pathScLauncher"
}
