$SCRIPTPATH = Split-Path $SCRIPT:MyInvocation.MyCommand.Path -parent
$ToBuildPath = "$SCRIPTPATH\base\src"
$converter = "$SCRIPTPATH\converter.ps1"

$version = "1.4"
$copyright = "https://guillian-aufrere.fr"

# Clean builded directory before.
Remove-Item "$SCRIPTPATH\builded\*.exe"

# Convert RSI Launcher for HOTAS PS1 source file.
ls "$ToBuildPath\RSI Launcher for HOTAS.ps1" | %{
	."$converter" "$($_.Fullname)" "$($_.Fullname -replace '.ps1','.exe')" `
    -verbose `
    -title "RSI Launcher for HOTAS" `
    -description "RSI Launcher for HOTAS" `
    -version "$version" `
    -product "Easy toggle RSI Launcher and your T.A.R.G.E.T profile." `
    -copyright "$copyright" `
    -iconFile "$SCRIPTPATH\base\media\hotas.ico"}

# Convert setup PS1 source file.
ls "$ToBuildPath\setup.ps1" | %{
	."$converter" "$($_.Fullname)" "$($_.Fullname -replace '.ps1','.exe')" `
    -verbose `
    -title "Launcher setup" `
    -description "Launcher setup" `
    -version "$version" `
    -product "Configure your RSI Launcher for HOTAS." `
    -copyright "$copyright" `
    -iconFile "$SCRIPTPATH\base\media\setup.ico"}


Remove-Item "$ToBuildPath\*.config"
Move-Item "$ToBuildPath\*.exe" "$SCRIPTPATH\builded"