# RSI Launcher for HOTAS

# [FR/fr]
Ce programme s’adresse à vous si vous êtes possesseur d’une matériel Thrustmaster et que vous l’utiliser depuis le logiciel T.A.R.G.E.T.

Si vous en avez marre de lancer T.A.R.G.E.T, sélectionner votre profile, lancer votre profile puis enfin ouvrir “RSI Launcher”.

Ce programme est fait pour vous !

## Installation

1. Télécharger la dernière version.
2. Extraire les fichiers contenus dans l’archive à la racine de votre dossier “Robert Space Industries”.
3. Lancer la configuration “setup.exe”
4. Des avertissements de sécurité peuvent apparaître à l’écran. Vous pouvez autoriser l’exécution en cliquant sur “Plus d’informations” puis “Exécuter quand même”. Ces messages apparaissent car Windows ne reconnais pas l’éditeur car je n’ai pas voulu payer une blinde, le certificat nécessaire.
5. Lancer “RSI Launcher for HOTAS”, un raccourci à été créé sur votre bureau.
6. Idem que dans l’étape 4.

Faîtes moi part de vos retours et de vos idées pour améliorer ce petit lanceur custom.

## Fonctionnalités

- Lancer votre profile T.A.R.G.E.T  puis le RSI Launcher automatiquement
- Attendre que T.A.R.G.E.T est finis de configurer les périphériques USB avant d’ouvrir le “RSI Launcher”.
- Arrête T.A.R.G.E.T et RSI Launcher si l’un des deux est en cours de fonctionnement.
- Sélection customisable du profil T.A.R.G.E.T voulu.
- Installeur graphique (setup.exe).
- Compatible avec d’autres jeux, il suffit de sélectionner le bon exécutable.

## Vidéo
Attention, la vidéo à été prise lors de la première réalisation du lanceur.
Les fonctionnalités peuvent êtres différentes depuis.
[Voir la vidéo](https://www.youtube.com/watch?v=rLK23MWW0kQ&fbclid=IwAR3jkJRWrzjcKp6rlO4Oo4niKneXx3iAT8naMKL-gX5mY3WyPXiQmXD8W64)

## Télécharger
[Télécharger la dernière version](https://hidrive.ionos.com/share/2lbwtbuoh3)

# [EN/en]

This script is an easy way to launch your RSI Launcher after HOTAS devides has been connected.

1. Double clic on RSI Launcher for HOTAS;
2. T.A.R.G.E.T profile is running;
3. After T.A.R.G.E.T has mounted your HOTAS devices;
4. RSI Launcher will automaticly start;
5. If you callback "RSI Launcher for HOTAS";
6. It will stop RSI Launcher and your devices.

# Developpers

Source files are located in base/src/ directory, you can work on it.
You can edit and re-compile your own application with "converter.ps1" library. This library will convert *.ps1 files to .exe.
I made a runner to help the call to this library. Just execute runBuild.ps1 with powershell and all your work will appear in builded directory.